$(document).ready(function(){
	var currentStep = 0; // 0 for welcome page, -1 for result page, others for question page
	var data = {};
	var answers = [];
	var quizFinished = false;

	// add some animation
	$(".quiz-index").fadeIn(1000);
	$(".quiz-content").fadeIn(1000); 

	// load json data
	$.getJSON("../data.json").then(function(result) {
		data = result;
		answers = new Array(data.questions.length).fill(0);
		renderPage(currentStep);
		renderIndex();
	})
	.catch(function(error){
	    console.log(error);
	});

	function renderIndex() {
		for (let i = 0; i < data.questions.length; i++) {
			$('.quiz-index').append('<div class="quiz-index-button d-block" question-index="' + (i + 1) + '">Question ' + (i + 1) + '</div>');
		}
	}

	function renderPage(currentStep) {
		switch(currentStep) {
			case 0:  // welcome
				$('.quiz-content').load('../templates/welcome.template.html', function() {
					$('#quiz-title').text(data.title);
					$('#quiz-introduction').text(data.welcomeMessage);
					$('#questionCount').text(data.questions.length);
					$('#passMark').text(data.passMark);
				});
				break;
			case -1:  // result
				let score = 0;
				for (let i = 0; i < data.questions.length; i++) {
					if (answers[i] === data.questions[i].correctAnswer)
						score += data.eachQuestionMark;
				}
				$('.quiz-content').load('../templates/result.template.html', function() {
					$('#quiz-title').text(data.title);
					$('#result-mark').text(score);
					$('#passOrFail').text(score >= data.passMark? 'Pass' : 'Fail');
					$('#passOrFail').addClass(score >= data.passMark? 'pass' : 'fail');
					$('#result-message').text(score >= data.passMark? data.passMessage : data.failMessage);
					for (let i = 0; i < data.questions.length; i++) {
						$.get('../templates/question-result.template.html', function(html) {
							$('#result-detail').append(html);
							$('.question-number').last().text(i + 1);
							$('.question-text').last().text(data.questions[i].question);
							let yourAnswer = data.questions[i].options.filter(function(option){return option.id === answers[i]})[0];
							$('.your-answer').last().text(yourAnswer.text);
							if (yourAnswer.id !== data.questions[i].correctAnswer) {
								let correctAnswer = data.questions[i].options.filter(function(option){return option.id === data.questions[i].correctAnswer})[0];
								$('.correct-answer-container').last().removeClass('d-none');
								$('.correct-answer').last().text(correctAnswer.text);
							}
						});
					}
				});
				break;
			default:  // question
				$('.quiz-content').load('../templates/question.template.html', function() {
					$('#quiz-title').text(data.title);
					$('#current-question-index').text(currentStep);
					$('#question-text').text(data.questions[currentStep - 1].question);
					$('#total-question-number').text(data.questions.length);
					let totalOptions = data.questions[currentStep - 1].options.length;
					for (let i = 0; i < totalOptions; i++) {
						$.get('../templates/question-option.template.html', function(html) {
							$('#question-options').append(html);
							$('.question-option').last().attr('option-index', data.questions[currentStep - 1].options[i].id);
							$('.question-option').last().text(data.questions[currentStep - 1].options[i].text);
							console.log(currentStep, answers[currentStep - 1]);
							if (answers[currentStep - 1] > 0) {
								if (answers[currentStep - 1] - 1 === i) {
									$('.question-option').last().addClass('selected');
									$('.link').attr('disabled', false);
								}
							}
						});
					}
				});
		}
	}

	// click start or submit button
	$('.quiz-content').delegate(".link", "click", function() {

		currentStep++;
		if(currentStep > data.questions.length) {
			currentStep = -1;
			quizFinished = true;
		}
		renderPage(currentStep);
		$('.quiz-index-button').removeClass('selected');
		$('.quiz-index-button:nth-of-type(' + currentStep + ')').addClass('selected');
	});

	// click question option
	$('.quiz-content').delegate(".question-option", "click", function() {
		answers[currentStep - 1] = parseInt($(this).attr('option-index'));
		$('.question-option').removeClass('selected');
		$(this).addClass('selected');
		$('.link').attr('disabled', false);
	});

	// click question index
	$('.quiz-index').delegate(".quiz-index-button", "click", function() {
		if (quizFinished)
			return 0;
		currentStep = $(this).attr('question-index');
		renderPage(currentStep);
		$('.quiz-index-button').removeClass('selected');
		if ($(this).attr('question-index') === currentStep)
			$(this).addClass('selected');
	});
})